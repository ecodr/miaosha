package com.peanut.miaosha.service;

import com.peanut.miaosha.domain.MiaoshaUser;
import com.peanut.miaosha.vo.LoginVo;

import javax.servlet.http.HttpServletResponse;

/**
 * @author LanceLee
 */
public interface MiaoshaUserService {

    public MiaoshaUser getByToken(HttpServletResponse response, String token);

    public boolean login(HttpServletResponse response, LoginVo loginVo);
}
