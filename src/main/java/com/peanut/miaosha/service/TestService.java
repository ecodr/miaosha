package com.peanut.miaosha.service;

import com.peanut.miaosha.domain.Test;

/**
 * @author LanceLee
 */
public interface TestService {

    public Test getTestById(int id);

    public boolean testTx();
}
