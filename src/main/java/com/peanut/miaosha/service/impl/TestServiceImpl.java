package com.peanut.miaosha.service.impl;

import com.peanut.miaosha.dao.TestMapper;
import com.peanut.miaosha.domain.Test;
import com.peanut.miaosha.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author LanceLee
 */
@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestMapper testMapper;

    @Override
    public Test getTestById(int id) {
        return testMapper.getById(id);
    }

    /**
     * 事物控制
     * @return
     */
//    @Transactional
    @Override
    public boolean testTx() {
        Test test1 = new Test();
        test1.setId(2);
        test1.setName("222222");
        testMapper.insert(test1);

        Test test2 = new Test();
        test2.setId(1);
        test2.setName("111111");
        testMapper.insert(test2);

        return true;
    }
}
