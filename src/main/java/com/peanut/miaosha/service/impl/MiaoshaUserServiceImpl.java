package com.peanut.miaosha.service.impl;

import com.peanut.miaosha.dao.MiaoshaUserMapper;
import com.peanut.miaosha.domain.MiaoshaUser;
import com.peanut.miaosha.exception.GlobalException;
import com.peanut.miaosha.redis.MiaoshaUserKey;
import com.peanut.miaosha.redis.RedisService;
import com.peanut.miaosha.result.CodeMsg;
import com.peanut.miaosha.service.MiaoshaUserService;
import com.peanut.miaosha.util.MD5Util;
import com.peanut.miaosha.util.UUIDUtil;
import com.peanut.miaosha.vo.LoginVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * @author LanceLee
 */
@Service
public class MiaoshaUserServiceImpl implements MiaoshaUserService {

    public static final String COOKIE_NAME_TOKEN = "token";

    @Autowired
    MiaoshaUserMapper miaoshaUserMapper;

    @Autowired
    RedisService redisService;

    /**
     * 根据id获取user
     * @param id
     * @return
     */
    public MiaoshaUser getById(long id) {
        return miaoshaUserMapper.getById(id);
    }

    /**
     * 根据token获取user
     * @param response
     * @param token
     * @return
     */
    @Override
    public MiaoshaUser getByToken(HttpServletResponse response, String token) {
        if (StringUtils.isEmpty(token)){
            return null;
        }

        MiaoshaUser user = redisService.get(MiaoshaUserKey.token, token, MiaoshaUser.class);

        //延长有效期
        if (user != null){
            addCookie(response, token, user);
        }
        return user;
    }

    /**
     *
     * @param response
     * @param loginVo
     * @return
     */
    @Override
    public boolean login(HttpServletResponse response, LoginVo loginVo) {
        if(loginVo == null) {
            throw new GlobalException(CodeMsg.SERVER_ERROR);
        }
        String mobile = loginVo.getMobile();
        String password = loginVo.getPassword();
        //判断手机号是否存在
        MiaoshaUser user = getById(Long.parseLong(mobile));
        if (user == null) {
            throw new GlobalException(CodeMsg.MOBILE_NOT_EXIST);
        }
        //验证密码
        String dbPassword = user.getPassword();
        String dbSalt = user.getSalt();
        String pass = MD5Util.formPassToDBPass(password, dbSalt);
        if (!pass.equals(dbPassword)) {
            throw new GlobalException(CodeMsg.PASSWORD_ERROR);
        }
        //生成cookie
        String uuid = UUIDUtil.uuid();
        addCookie(response, uuid, user);
        return true;
    }

    /**
     * 添加cookie
     * @param response
     * @param token
     * @param user
     */
    private void addCookie(HttpServletResponse response, String token, MiaoshaUser user) {
        redisService.set(MiaoshaUserKey.token, token, user);
        Cookie cookie = new Cookie(COOKIE_NAME_TOKEN, token);
        cookie.setMaxAge(MiaoshaUserKey.token.expireSeconds());
        cookie.setPath("/");
        response.addCookie(cookie);
    }
}
