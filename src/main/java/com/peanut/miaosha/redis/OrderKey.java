package com.peanut.miaosha.redis;

/**
 * @author LanceLee
 */
public class OrderKey extends BasePrefix {

    public OrderKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }
}
