package com.peanut.miaosha.redis;

/**
 * @author LanceLee
 */
public interface KeyPrefix {

    public int expireSeconds();

    public String getPrefix();
}
