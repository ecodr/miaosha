package com.peanut.miaosha.result;

/**
 * 成功/失败返回
 * @author LanceLee
 */
public class Result<T> {

    private int code;
    private String msg;
    private T data;

    /**
     * 成功时调用
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(T data){
        return new Result<T>(data);
    }

    /**
     * 失败时调用
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(CodeMsg msg){
        return new Result<T>(msg);
    }

    private Result(CodeMsg msg) {
        if (msg == null){
            return;
        }
        this.code = msg.getCode();
        this.msg = msg.getMsg();
    }

    private Result(T data){
        this.code = 0;
        this.msg = "success";
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }

}
