package com.peanut.miaosha.controller;

import com.peanut.miaosha.domain.Test;
import com.peanut.miaosha.result.CodeMsg;
import com.peanut.miaosha.result.Result;
import com.peanut.miaosha.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author LanceLee
 */
@Controller
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping("/template")
    public String testTemplate(Map<String, Object> map){
        map.put("hello", "August Rush");
        return "test";
    }

    @RequestMapping("/db/get")
    @ResponseBody
    public Result<Test> getTest(){
        Test test = testService.getTestById(1);
        return Result.success(test);
    }

    @RequestMapping("/err")
    @ResponseBody
    public Result<String> error(){
        return Result.error(CodeMsg.SERVER_ERROR);
    }

    @RequestMapping("/testTx")
    @ResponseBody
    public Result<Boolean> testTx(){
        testService.testTx();
        return Result.success(testService.testTx());
    }
}
