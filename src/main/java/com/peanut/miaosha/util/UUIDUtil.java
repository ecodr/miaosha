package com.peanut.miaosha.util;

import java.util.UUID;

/**
 * @author LanceLee
 */
public class UUIDUtil {

    public static String uuid(){
        return UUID.randomUUID().toString().replace("-", "");
    }
}
